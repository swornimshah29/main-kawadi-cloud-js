


const  firebaseFirestore=require('firebase-admin');
const  distance_matrix =require('google-distance-matrix');
const functions=require('firebase-functions')

firebaseFirestore.initializeApp(functions.config().firebase)
distance_matrix.key('AIzaSyBoamspBjGAp6mltFLFDx-aNA8cLetyvLk')

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

let mainCounter=0
let wasteStack=[]
let wasteNodeList=[]


let nextNode
let pVisitedNodes=[]
let tVisitedNodes=[]
let tVisitedNodesII=[]//second phase
let recommendedWastes=[]//all the wastes from waste node of firestore



var Waste = /** @class */ (function () {
 
    function Waste( SourceLat, SourceLon, SourceId, SourceStatus,SourceType,Distance,Duration,Paths) {
        this.sourceId = SourceId;
        this.sourceLat = SourceLat;
        this.sourceLon = SourceLon;
        this.sourceStatus = SourceStatus;
        this.distance=Distance;
        this.duration=Duration;
        this.sourceType=SourceType;
        this.paths=Paths
        
    }
    return Waste;
}());

var WasteNode = /** @class */ (function () {
    //this is used by nearby wastes only
    function WasteNode(WasteList,SourceId) {
        this.wasteList=WasteList
        this.sourceId=SourceId
    }
    return WasteNode;
}());

var Truck = /** @class */ (function () {

    function Truck(TruckPosLat,TruckPosLon,TruckId,TruckDriverName,TruckDriverPnumber,Truckwastes,Status,Distance,Duration) {
        this.truckPosLat=TruckPosLat
        this.truckPosLon=TruckPosLon
        this.truckId=TruckId;
        this.truckDriverName=TruckDriverName;
        this.truckDriverPnumber=TruckDriverPnumber;
        this.truckwastes=Truckwastes;
        this.status=Status;
        this.distance=Distance;
        this.duration=Duration;



    }
    return Truck;
}());

let pVisitedNodesPromise=function(wasteStack){
     return new Promise((resolve,reject)=>{
         console.log("pvisitednodes promies")
        let tempwasteStack=wasteStack//dont use wastestack
        tempwasteStack.forEach((tempEach,index)=>{
            console.log("temp waste" +tempEach.sourceId)
            pVisitedNodes.push(tempEach)
        })

        if(pVisitedNodes.length>0){resolve()}
     })

}


exports.nearbywaste=functions.firestore.document("pickers/{wildCard}/nearbyplaces/nearbyplaceDoc").onWrite(event=>{
    const nearByplaceDoc=event.data.data()
    if(nearByplaceDoc.done){
        return
    }
    console.log("Request came from a truck driver "+nearByplaceDoc.truckId)
    nearByplaceDoc.done=true

    const truckId=nearByplaceDoc.truckid//get the truckid

    return event.data.ref.set(nearByplaceDoc).then(res=>{
        return   firebaseFirestore.firestore().collection('wastes').get().then(snapshot=>{
            console.log("snapshot  "+JSON.stringify(snapshot))
            
            var destinations=[]
            var destinations2=[]
            let wasteStackRank=[] //container of objects of type waste
            let nearbyWasteStack=[]//top nearest among many
            let eachWaste
            let origins=['27.688850,85.314870']
            
            snapshot.forEach((eachDoc)=>{
                // console.log("wastes id "+eachDoc.id)
                // console.log("wastes data "+eachDoc.data())
                recommendedWastes.push(eachDoc.data())
                destinations.push(eachDoc.data().sourceLat+","+eachDoc.data().sourceLon)
                destinations2.push(eachDoc.data().sourceLat+","+eachDoc.data().sourceLon)
                
                console.log("each destinations"+destinations.pop())
                

            })            
            var data={
                foo:recommendedWastes
            };

            //phase I -shoretest nodes in given location

            return new Promise((resolve,reject)=>{
                distance_matrix.matrix(origins,destinations2,function(err,response){
                    console.log("json response "+JSON.stringify(response))
                    if(response['status']=="OK"){
                        var rowsObject=response['rows']
                        rowsObject.forEach(eachROws=>{
                            var elements=eachROws['elements']
                            elements.forEach((eachElements,index)=>{ //two asrgs value and counter as eachelements and index
                                if(eachElements.status=="OK"){
                
            
                            //check if the order is same as java object and what if error comes
                            eachWaste=new Waste(
                                recommendedWastes[index].sourceLat,
                                recommendedWastes[index].sourceLon,
                                recommendedWastes[index].sourceId,
                                "N",
                                recommendedWastes[index].sourceType,
                                eachElements.distance.value,
                                eachElements.duration.value,
                                recommendedWastes[index].paths
                                
                                )
                            wasteStack.push(eachWaste)//hold the result for that object temporary
                            }
                               
                           })
                        })
                        resolve()
                   }else{
                       console.log("error in the distance matrix calls")
                   }
                })
            })
               
        }).then(()=>{
            console.log("distance matrix call succeeded")
            chainAlgorithmPhases(wasteStack)
        })
               
    })
})



exports.nearbywasteRequest=functions.firestore.document("nearbyRequest/{wildCard}").onWrite(event=>{
    /*listen to any new request from the picker*/

    let requestOrigin=[]
    let truckStack=[]
    
    let nearbyPickersDestinations=[]
    
    let recommendedTrucks=[]
    let newOldRequestDoc=event.data.data()
    requestOrigin.push(newOldRequestDoc.waste.sourceLat+","+newOldRequestDoc.waste.sourceLon)
    console.log("nearbyrequest is starting for "+newOldRequestDoc.truckId)
    console.log("nearbyrequest is starting for "+JSON.stringify(event.data.data()))
    console.log("nearbyrequest origin "+JSON.stringify(requestOrigin[0]))
    
    if(newOldRequestDoc.status){
        /*send the notification to driver saying his/her job has been handled*/
        return

    }

    //else perform the searching of nearby driver and notify him or her for new task
    return  firebaseFirestore.firestore().collection('registerDrivers').get().then(snapshot=>{
        
        snapshot.forEach((each)=>{
            if(each.data().truckId!=newOldRequestDoc.truckId){
            recommendedTrucks.push(each.data())
            }
        })

        //all pickers except one who requested
        recommendedTrucks.forEach((each)=>{
            nearbyPickersDestinations.push(each.truckPosLat+","+each.truckPosLon)
            
        })
        console.log("Distance matrix for nearbyRequest")
        return new Promise((resolve,reject)=>{
            let eachTruck
            distance_matrix.matrix(requestOrigin,nearbyPickersDestinations,function(err,response){
                console.log("json response "+JSON.stringify(response))
                if(response['status']=="OK"){
                    var rowsObject=response['rows']
                    rowsObject.forEach(eachROws=>{
                        var elements=eachROws['elements']
                        elements.forEach((eachElements,index)=>{ //two asrgs value and counter as eachelements and index
                            if(eachElements.status=="OK"){
            
        
                        //check if the order is same as java object and what if error comes
                        eachTruck=new Truck(
                            recommendedTrucks[index].truckPosLat,
                            recommendedTrucks[index].truckPosLon,
                            recommendedTrucks[index].truckId,
                            null,
                         null,
                         null,
                         null,
                         eachElements.distance.value,
                         eachElements.duration.value
                            
                            )
                        truckStack.push(eachTruck)//hold the result for that object temporary
                        }
                           
                       })
                    })

                    resolve("success")
                }
            })
        })
    }).then((message)=>{
        console.log(message)
        truckStack.sort((truck1,truck2)=>{
            return truck1.distance-truck2.distance
        })
        console.log("truck nearby are ")
        truckStack.forEach((each)=>{
            console.log("nearby trucks are "+each.truckId)
        })

        newOldRequestDoc.status=true
        
        return event.data.ref.set(newOldRequestDoc).then(res=>{

        })

})


})

    





function chainAlgorithm(wasteStack){

    let eachWaste
    let currentWasteNode

    console.log("chain algorithm started")

    //get the actual data(JAVA OBJECT) on the basis of sourceId from recommendedWastes
    let tempwasteStack=wasteStack//dont use wastestack
    tempwasteStack.forEach((tempEach,index)=>{
        console.log("temp waste" +tempEach.sourceId)
        recommendedWastes.forEach((recomEach)=>{
            if(index<5){
                pVisitedNodes.push(recomEach)
                console.log("js"+JSON.stringify(tempEach))
            }
            })
  
    })

    pVisitedNodes.forEach((each)=>{
        console.log("pvisited id"+each.sourceId)
    })
    /*Second phase startes with recursive functions*/

   
    // pVisitedNodes.forEach((nearbyObject,index) => {
    //     //sort first 3 or 5 excluding pv and tv
    //     console.log("started loop "+index)
        
    //     let eachObject=new Path(nearbyObject)//each nearbyobject with additional informations like paths and all
    //     console.log("java"+JSON.stringify(nearbyObject))
        
    //     tVisitedNodesII.push(eachObject)
    //     console.log("promise starting")
    //     callRecursiveChainalgorithm(eachObject)
    //     console.log("started "+index)

    // })

    AysncCallRecursiveChain(mainCounter)
}


function callRecursiveChainalgorithm(eachObject){
    let currentWasteNode
    let eachWaste
    console.log("recursive code body")
        currentWasteNode=eachObject//nearest object from location #js class of waste#needed only for nearest top 5
        let currentWasteNodeOrigin=[]
        let rvisitingWasteNodeDestination=[]
        let tempwasteStackII=[]
        
        rVisitingNodes=rVisitingNodesRecommendation()//new rvisting nodes for each nearbyobject
        //make a lat and lot for currentwastenodeorigin
        //extract lat lon of arrays from rvisitingnodes object and store to rvisitingwastenodedestination
        currentWasteNodeOrigin.push(currentWasteNode.waste.sourceLat+","+currentWasteNode.waste.sourceLon)
        rVisitingNodes.forEach((each)=>{
            rvisitingWasteNodeDestination.push(each.sourceLat+","+each.sourceLon)
        })

        distance_matrix.matrix(currentWasteNodeOrigin,rvisitingWasteNodeDestination,function(err,response){
            if(response['status']=="OK"){
                var rowsObject=response['rows']
                rowsObject.forEach(eachROws=>{
                    var elements=eachROws['elements']
                    elements.forEach((eachElements,index)=>{ //two args value and counter as eachelements and index
                        if(eachElements.status=="OK"){
                            eachWaste=new WasteNode(eachElements.distance.value,eachElements.duration.value,recommendedWastes[index].sourceId,"N")//create new waste everytime
                            tempwasteStackII.push(eachWaste)
                        }
                       
                   })
                })           
             }
        })

        /*----------------begin nearby implementation------------------------------------*/
      
        /*perform sorting i.e top 5 nearest node*/
        tempwasteStackII.sort(function(waste1, waste2){return waste1.distance-waste2.distance});
        //find the smallest node
        let smallestNode//java object waste
        
        recommendedWastes.forEach((each)=>{
            if(tempwasteStackII[0].sourceId==each.sourceId){
                smallestNode=each
            }
        })
        console.log("smallest node is "+smallestNode)
        
        if(smallestNode!=null){
            tVisitedNodesII.push(smallestNode)
        }

        eachObject.pathMap.push(smallestNode)
        if(eachObject.pathMap.length<=2){
            //call recursive again for the same object
            console.log("recursive 2 times")
            callRecursiveChainalgorithm(eachObject)
        }else{
            //empty all arrays for next loop
            tVisitedNodesII=[]
            rVisitingNodes=[]
        }
                                        
}


function rVisitingNodesRecommendation(){
    
        let ttvisitedNodes=[]//stores only sourceid of tvisitednodes
        let ppvisitedNodes=[]//same
        let trVisitingNodes=[]
        if(trVisitingNodes.length>0){
            trVisitingNodes=[]
        }
        
        tVisitedNodesII.forEach((each)=>{
            ttvisitedNodes.push(each.sourceId)//object cannot be compared so taking its sourceId array
        })
    
    
        pVisitedNodes.forEach((each)=>{
            ppvisitedNodes.push(each.sourceId)
        })

        for(var i=0;i<recommendedWastes.length;i++){
                if(!ttvisitedNodes.includes(recommendedWastes[i].sourceId) && !ppvisitedNodes.includes(recommendedWastes[i].sourceId)){
                    trVisitingNodes.push(recommendedWastes[i])//stores object of java class#actual data
                }
            
        }

        return trVisitingNodes
    
    }
    

                

function AysncCallRecursiveChain(mainCounter){

    if(mainCounter==3){
        return new Promise((resolve,reject)=>{
            pVisitedNodes.forEach((each)=>{
                each.paths.forEach((path,index)=>{
                    console.log("paths of "+each.sourceId+" "+path.sourceId)
    
                })
            })
            resolve()
        }).then(()=>{
            console.log("ending the recursive")
            return//close the program
        })
    }

    return  new Promise((resolve,reject)=>{
        /*for each wastes*/
    /*also check the tv and pv*/

    let currentWasteNodeOrigin=[]
    let rvisitingWasteNodeDestination=[]
    let tempwasteStackII=[]
    let rVisitingNodes=[]//dont include pVisited and tVisited #recommended visiting nodes
    
    /*from the pv[object] get the locations and its recommendatios*/

    if(tVisitedNodesII.length==0){
        tVisitedNodesII.push(pVisitedNodes[mainCounter])
        nextNode=tVisitedNodesII[tVisitedNodesII.length-1]
    }else{
        nextNode=tVisitedNodesII[tVisitedNodesII.length-1]
    }

    rVisitingNodes=rVisitingNodesRecommendation()//all except pvisited and tvisited nodes
    currentWasteNodeOrigin.push(nextNode.sourceLat+","+nextNode.sourceLon)
    rVisitingNodes.forEach((each)=>{
        rvisitingWasteNodeDestination.push(each.sourceLat+","+each.sourceLon)
    })
    
    
    distance_matrix.matrix(currentWasteNodeOrigin,rvisitingWasteNodeDestination,function(err,response){
        if(response['status']=="OK"){
            console.log("distance matrix first")
            var rowsObject=response['rows']
            rowsObject.forEach(eachROws=>{
                var elements=eachROws['elements']
                elements.forEach((eachElements,index)=>{ //two args value and counter as eachelements and index
                    if(eachElements.status=="OK"){
                        eachWaste=new Waste(
                            recommendedWastes[index].sourceLat,
                            recommendedWastes[index].sourceLon,
                            recommendedWastes[index].sourceId,
                            "N",
                            recommendedWastes[index].sourceType,
                            eachElements.distance.value,
                            eachElements.duration.value,
                            null
                            )                        
                        tempwasteStackII.push(eachWaste) ///java object
                    }
                   
               })
            })  
            resolve(tempwasteStackII)         
         }
    })

    }).then((tempwasteStackII)=>{
        /*sort ascending and find the shorestest node*/
        tempwasteStackII.sort(function(waste1, waste2){return waste1.distance-waste2.distance});
        //find the smallest node
        let smallestNode//java object waste
        
        for(var i=0;i<recommendedWastes.length;i++){
            if(tempwasteStackII[0].sourceId==recommendedWastes[i].sourceId){
                smallestNode=recommendedWastes[i]
                tempwasteStackII=[]
                currentWasteNodeOrigin=[]
                rvisitingWasteNodeDestination=[]
                rVisitingNodes=[]
                break;
            }
        }    
        tVisitedNodesII.push(smallestNode)


        pVisitedNodes[mainCounter].paths.push(smallestNode)
        if(pVisitedNodes[mainCounter].paths.length<=4){
            //call recursive again for the same object
            AysncCallRecursiveChain(mainCounter)
        }else{
            //empty all arrays for next loop
            tVisitedNodesII=[]
            ++mainCounter
            AysncCallRecursiveChain(mainCounter)
            
        }

    })
}

let recursivePromisePhaseI=function (mainCounter){


    return new Promise((resolve,reject)=>{

        console.log("reading distance matrix")
        let currentWasteNodeOrigin=[]
        let rvisitingWasteNodeDestination=[]
        let rvisitingPreviousNodes=[]
        
        let rvi
        let tempwasteStackII=[]
        let rVisitingNodes=[]//dont include pVisited and tVisited #recommended visiting nodes
        let eachWaste
        let wasteNode
        
        /*from the pv[object] get the locations and its recommendatios*/
        console.log("counter value "+mainCounter)
        if(pVisitedNodes.length==mainCounter){
            resolve("finish")
        }
        currentWasteNodeOrigin.push(pVisitedNodes[mainCounter].sourceLat+","+pVisitedNodes[mainCounter].sourceLon)
        recommendedWastes.forEach((each)=>{
            if(each.sourceId!=pVisitedNodes[mainCounter].sourceId){
                rvisitingPreviousNodes.push(each)
                rvisitingWasteNodeDestination.push(each.sourceLat+","+each.sourceLon)
            }            
        })

        distance_matrix.matrix(currentWasteNodeOrigin,rvisitingWasteNodeDestination,function(err,response){
            if(response['status']=="OK"){
                var rowsObject=response['rows']
                rowsObject.forEach(eachROws=>{
                    var elements=eachROws['elements']
                    elements.forEach((eachElements,index)=>{ //two args value and counter as eachelements and index
                        if(eachElements.status=="OK"){
                                //dont let current object Id also get into the stack
                                eachWaste=new Waste(

                                rvisitingPreviousNodes[index].sourceLat,
                                rvisitingPreviousNodes[index].sourceLon,
                                rvisitingPreviousNodes[index].sourceId,
                                rvisitingPreviousNodes[index].sourceStatus,
                                rvisitingPreviousNodes[index].sourceType,
                                eachElements.distance.value,
                                eachElements.duration.value,
                                rvisitingPreviousNodes[index].paths
                                
                                )           
                                         
                            tempwasteStackII.push(eachWaste) ///java object
                    
                        }
                       
                   })
                }) 
                console.log("FROM "+pVisitedNodes[mainCounter].sourceId+" : "+JSON.stringify(response))
                //create a new WasteNode object
                
                wasteNode=new WasteNode(tempwasteStackII,pVisitedNodes[mainCounter].sourceId)
                wasteNodeList.push(wasteNode) 
                if(pVisitedNodes.length!=mainCounter){
                    recursivePromisePhaseI(++mainCounter)
                }
             }
        })

    }).then((message)=>{
        console.log("First phase has "+message)
        recursivePromisePhaseII()

    }).catch((message)=>{
        console.log("First phase has error as"+message)
    })
    
}

let recursivePromisePhaseII=function(){

    console.log("Second phase has started")
    
    /*Now sort pVisistedNodes #Sorting is done with reference to location provided by truckdriver*/
    pVisitedNodes.sort(function(waste1,waste2){
        return waste1.distance-waste2.distance
    })

    /*sort the wasteNodeList[n].wasteList on the basis of distance*/
    wasteNodeList.forEach((each1)=>{
        each1.wasteList.sort((waste1,waste2)=>{
            return waste1.distance-waste2.distance
        })
    })


    console.log("Sorting is done")
    console.log("overall obj "+JSON.stringify(wasteNodeList[0]))
    console.log("inner obj wastelist "+JSON.stringify(wasteNodeList[0].wasteList))
    
    for(var i=0;i<3;i++){//each top 3 nearby with location nodes with index i
        
        for(var j=0;j<wasteNodeList.length;j++){//each neighbouring nodes with index j
            
            if(pVisitedNodes[i].paths.length==3){break}
            for(var k=0;k<wasteNodeList[j].wasteList.length;k++){ //each inner nodes for each j as k                
                if(wasteNodeList[j].wasteList[k].sourceId==pVisitedNodes[i].sourceId){
                    pVisitedNodes[i].paths.push(wasteNodeList[j])
                    break;
                }

                
            }
        
        
        }
    
    
    }
    console.log("paths for "+pVisitedNodes[1].sourceId)
    pVisitedNodes[1].paths.forEach((each)=>{
        console.log("paths are "+each.sourceId)
        
    })

}


function chainAlgorithmPhases(wasteStack){
    // all the task like finding and recursive are done here
    pVisitedNodesPromise(wasteStack).then(()=>{
        recursivePromisePhaseI(mainCounter)
    })
}



function nearbyWastes(){
    let ttvisitedNodes=[]//stores only sourceid of tvisitednodes
    let ppvisitedNodes=[]//same
    let trVisitingNodes=[]
    if(trVisitingNodes.length>0){
        trVisitingNodes=[]
    }
    
    tVisitedNodesII.forEach((each)=>{
        console.log("each recommen json"+JSON.stringify(each))
        ttvisitedNodes.push(each.sourceId)//object cannot be compared so taking its sourceId array
    })


    pVisitedNodes.forEach((each)=>{
        ppvisitedNodes.push(each.sourceId)
    })

    for(var i=0;i<recommendedWastes.length;i++){
            if(!ttvisitedNodes.includes(recommendedWastes[i].sourceId)){
                trVisitingNodes.push(recommendedWastes[i])//stores object of java class#actual data
            }
        
    }

    return trVisitingNodes


}


